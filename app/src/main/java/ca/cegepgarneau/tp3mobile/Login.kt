package ca.cegepgarneau.tp3mobile

import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import ca.cegepgarneau.tp3mobile.SharedPreferences.MyPreferences
import ca.cegepgarneau.tp3mobile.config.mapActivity
import ca.cegepgarneau.tp3mobile.data.model.User
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class Login : AppCompatActivity() {
    lateinit var editTextEmail: TextInputEditText
    lateinit var editTextPassword: TextInputEditText
    lateinit var buttonConnexion: Button
    lateinit var mAuth: FirebaseAuth
    lateinit var tview: TextView
    val db = Firebase.firestore

    @Override
    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = mAuth.currentUser
        if (currentUser != null) {
            intent = Intent(applicationContext, Register::class.java)
            startActivity(intent)
            finish()
        }
    }

    /**
     * création de l'activité
     */
    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val mypreferences = MyPreferences(this)

        // connexion a firebase authentification
        mAuth = FirebaseAuth.getInstance()
        editTextEmail = findViewById<TextInputEditText>(R.id.email)
        editTextPassword = findViewById(R.id.passwod)
        buttonConnexion = findViewById(R.id.btn_login)
        tview = findViewById(R.id.registerNow)

        // click sur bouton créer compte
        tview.setOnClickListener {
            intent = Intent(applicationContext, Register::class.java)
            startActivity(intent)
            finish()
        }

        // click sur bouton connexion
        buttonConnexion.setOnClickListener{
            var email = editTextEmail.text.toString()
            var password = editTextPassword.text.toString()

            if (email.isEmpty()) {
                Toast.makeText(this, "Entrer un courriel", Toast.LENGTH_SHORT).show()
            }
            if (password.isEmpty()) {
                Toast.makeText(this, "Entrer un mot de passe", Toast.LENGTH_SHORT).show()
            }

            // ajout du user dans firebase
            db.collection("users").whereEqualTo("email", email).get()
                .addOnSuccessListener { result ->
                    if (result.isEmpty){
                        Toast.makeText(this, "Erreur dans l'authentification", Toast.LENGTH_SHORT).show()
                    }
                    for (document in result) {
                        val passwordEmail = document.data["password"]
                        if (passwordEmail == password) {

                            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                            imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)

                            intent = Intent(applicationContext, mapActivity::class.java)
                            startActivity(intent)
                            finish()
                            // Sign in success, update UI with the signed-in user's information
                            val user = mAuth.currentUser
                        }else{
                            Toast.makeText(this, "Erreur dans l'authentification", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
        }

    }
}