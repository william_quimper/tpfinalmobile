package ca.cegepgarneau.tp3mobile.config

import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import ca.cegepgarneau.tp3mobile.MainActivity
import ca.cegepgarneau.tp3mobile.R
import ca.cegepgarneau.tp3mobile.Register
import ca.cegepgarneau.tp3mobile.SharedPreferences.MyPreferences
import ca.cegepgarneau.tp3mobile.data.model.User
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import java.io.IOException

class ConfigActivity : AppCompatActivity() {
    var isPlaying = false
    var mediaPlayer: MediaPlayer? = null

    /**
     * Survient à la création de l'activité
     */
    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_config)

        val editTextFN: EditText = findViewById(R.id.editTextFirstName)
        val editTextLN: EditText = findViewById(R.id.editTextLastName)
        val buttonConfirmer: Button = findViewById(R.id.buttonSubmit)
        val buttonJouer: Button = findViewById(R.id.buttonJouer)

        // Shared Preferences
        val mypreferences = MyPreferences(this)
        val _user = mypreferences.getLoginAccount()
        // Afficher le nom dans les textview
        editTextFN.setText(_user.name)
        editTextLN.setText(_user.lastName)

        // Action lors du click sur le bouton confirmer
        buttonConfirmer.setOnClickListener {
            val first_name = editTextFN.text.toString()
            val last_name = editTextLN.text.toString()
            val email = _user.email
            val id = _user.id
            val userModified = User(id, first_name, last_name, email)
            mypreferences.setUser(userModified)

            intent = Intent(applicationContext, mapActivity::class.java)
            startActivity(intent)
            finish()
        }

        // click sur le bouton jouer musique
        buttonJouer.setOnClickListener {

            if (isPlaying) {
                stopMusic()
            } else {
                startMusic()
            }
        }
    }

    // jouer la musique
    private fun startMusic() {
        mediaPlayer = MediaPlayer.create(this, R.raw.musique)
        mediaPlayer?.start()
        isPlaying = true
    }

    // arreter la musique
    private fun stopMusic() {
        mediaPlayer?.stop()
        mediaPlayer?.release()
        mediaPlayer = null
        isPlaying = false
    }

}