package ca.cegepgarneau.tp3mobile

import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import ca.cegepgarneau.tp3mobile.SharedPreferences.MyPreferences
import ca.cegepgarneau.tp3mobile.config.mapActivity
import ca.cegepgarneau.tp3mobile.data.model.User
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

public class Register : AppCompatActivity() {

    lateinit var editTextEmail: TextInputEditText
    lateinit var editTextPassword: TextInputEditText
    lateinit var buttonCreer: Button
    lateinit var mAuth: FirebaseAuth
    lateinit var tview: TextView
    val db = Firebase.firestore
    lateinit var mypreferences: MyPreferences


    /**
     * création de l'activité
     */
    @Override
    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = mAuth.currentUser

        //si le user est connecté on affiche la map
        if (currentUser != null) {
            intent = Intent(applicationContext, mapActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        setContentView(R.layout.activity_register)
        editTextEmail = findViewById<TextInputEditText>(R.id.email)
        editTextPassword = findViewById(R.id.passwod)
        buttonCreer = findViewById(R.id.btn_register)
        tview = findViewById(R.id.loginNow)

        mypreferences = MyPreferences(this)

        tview.setOnClickListener {
            intent = Intent(applicationContext, Login::class.java)
            startActivity(intent)
            finish()
        }


        buttonCreer.setOnClickListener {
            var email = editTextEmail.text.toString()
            var password = editTextPassword.text.toString()

            if (email.isEmpty()) {
                Toast.makeText(this, "Entrer un courriel", Toast.LENGTH_SHORT).show()
            }
            if (password.isEmpty()) {
                Toast.makeText(this, "Entrer un mot de passe", Toast.LENGTH_SHORT).show()
            }

            val user = hashMapOf(
                "email" to email,
                "password" to password,
            )
            db.collection("users").add(user)

            val userpref = User("a", "cegep", "garneau", email)
            mypreferences.setUser(userpref)

            Toast.makeText(
                baseContext,
                "Création du compte réussie.",
                Toast.LENGTH_SHORT,
            ).show()
            intent = Intent(applicationContext, Login::class.java)
            startActivity(intent)
            finish()
        }
    }
}