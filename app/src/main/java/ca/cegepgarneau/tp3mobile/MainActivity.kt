package ca.cegepgarneau.tp3mobile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import android.widget.Toolbar
import androidx.navigation.ui.AppBarConfiguration
import ca.cegepgarneau.tp3mobile.SharedPreferences.MyPreferences
import ca.cegepgarneau.tp3mobile.config.ConfigActivity
import ca.cegepgarneau.tp3mobile.config.mapActivity
import ca.cegepgarneau.tp3mobile.data.model.User
import ca.cegepgarneau.tp3mobile.databinding.ActivityMainBinding
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.lang.Override

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    var user : FirebaseUser? = null
    lateinit var buttonDeconnexion : Button
    lateinit var tview : TextView
    val db = Firebase.firestore
    lateinit var idUserConnected : String
    lateinit var _user: User

    /**
     * création de l'activité
     */
    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        buttonDeconnexion = findViewById(R.id.btn_logout)
        tview = findViewById(R.id.user_details)

        val mypreferences = MyPreferences(this)
        _user = mypreferences.getLoginAccount()

        // set tu helloworld pour le email
        tview.setText(_user.email)

        //click sur le bouton déconnexion
        buttonDeconnexion.setOnClickListener{
            mypreferences.logout()
            intent = Intent(applicationContext, Login::class.java)
            startActivity(intent)
            finish()
        }
        // click sur le bouton map
        findViewById<Button>(R.id.map).setOnClickListener {
            intent = Intent(applicationContext, mapActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
    // Initialisation du menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    /**
     * Handler pour les items sélectionnés
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // click sur l'action config
        when(item.itemId){
            R.id.action_config -> {
                intent = Intent(this, ConfigActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return false
    }
}