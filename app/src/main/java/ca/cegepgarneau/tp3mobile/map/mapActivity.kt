package ca.cegepgarneau.tp3mobile.config

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import android.media.AudioManager
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import ca.cegepgarneau.tp3mobile.MainActivity
import ca.cegepgarneau.tp3mobile.R
import ca.cegepgarneau.tp3mobile.Register
import ca.cegepgarneau.tp3mobile.SharedPreferences.MyPreferences
import ca.cegepgarneau.tp3mobile.data.model.Message
import ca.cegepgarneau.tp3mobile.data.model.User
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import java.io.InputStream
import kotlin.concurrent.thread

class mapActivity : AppCompatActivity(), GoogleMap.OnMarkerClickListener {

    lateinit var mapFragment : SupportMapFragment
    lateinit var googleMap : GoogleMap
    val LOCATION_PERMISSION_CODE = 1

    // pour enregistrer la position de l'utilisateur
    lateinit var currentLocation: Location

    lateinit var fusedLocation : FusedLocationProviderClient

    private lateinit var locationRequest: LocationRequest

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        fusedLocation = LocationServices.getFusedLocationProviderClient(this)

        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync{googleMaps ->
            googleMap = googleMaps
        }

        enableMyLocation()

        val mypreferences = MyPreferences(this)
        val _user = mypreferences.getLoginAccount()

    }


    private fun enableMyLocation() {
        // vérification si la permission de localisation est déjà donnée
        if (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED
        ) {
            mapFragment.getMapAsync {
                googleMap = it
                googleMap.setOnMarkerClickListener(this)
                // Permet d'afficher le bouton pour centrer la carte sur la position de l'utilisateur
                googleMap.isMyLocationEnabled = true
                googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                val lating = LatLng(3.95319, 38.14480)
                val markerOptions = MarkerOptions().position(lating).title("Localisation")
                googleMap?.animateCamera(CameraUpdateFactory.newLatLng(lating))
                googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(lating, 7f))
                googleMap.addMarker(markerOptions)

                onMapReady(googleMap)
            }
        } else {
            // La permission est manquante, demande donc la permission
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_CODE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Autorisation accordée", Toast.LENGTH_LONG).show()
                enableMyLocation()
            } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                ) {
                    val dialog = AlertDialog.Builder(this)
                    dialog.setTitle("Permission requise !")
                    dialog.setMessage("Cette permission est importante pour la géolocalisation...")
                    dialog.setPositiveButton(
                        "Ok"
                    ) { dialog, which ->
                        requestPermissions(
                            arrayOf(
                                Manifest.permission.ACCESS_FINE_LOCATION
                            ), LOCATION_PERMISSION_CODE
                        )
                    }
                    dialog.setNegativeButton(
                        "Annuler"
                    ) { dialog, which ->
                        Toast.makeText(
                            this,
                            "Impossible de vous localiser",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    dialog.show()
                }
            }
        }
    }

    fun onMapReady(googleMap: GoogleMap){
        googleMap.setOnMapClickListener { latLng ->
            Toast.makeText(
                applicationContext,
                "Clicked: Lat = ${latLng.latitude}, Lng = ${latLng.longitude}",
                Toast.LENGTH_SHORT
            ).show()
            googleMap.addMarker(MarkerOptions().position(latLng).title("Marker"))
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13f))

        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        // Step 5: Create custom InfoWindowAdapter
        googleMap.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            override fun getInfoContents(marker: Marker): View? {
                // Step 6: Inflate custom layout for info window
                val view = layoutInflater.inflate(R.layout.marker_layout, null)
                val tvAuthor = view.findViewById<TextView>(R.id.tv_author)
                val tvMessage = view.findViewById<TextView>(R.id.tv_message)
                val iv = view.findViewById<ImageView>(R.id.imageView)
                tvAuthor.text = marker.title
                val message: Message? = marker.tag as Message?
                tvMessage.text = if (message != null) message.message else null

                runOnUiThread {
                    val client = OkHttpClient()
                    val request = Request.Builder()
                        .url("https://robohash.org/hahaH")
                        .build()
                    val response = client.newCall(request).execute()
                    if (response.isSuccessful) {
                        val inputStream: InputStream? = response.body?.byteStream()
                        val bitmap = BitmapFactory.decodeStream(inputStream)
                        // Update the ImageView in the main thread using a Handler:
                        iv.setImageBitmap(bitmap)
                    }
                }
                return view
            }

            override fun getInfoWindow(marker: Marker): View? {
                return null
            }
        })

        marker.showInfoWindow()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){
            R.id.action_config -> {
                intent = Intent(this, ConfigActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return false
    }

}