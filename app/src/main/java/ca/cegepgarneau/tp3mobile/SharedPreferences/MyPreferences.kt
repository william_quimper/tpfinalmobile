package ca.cegepgarneau.tp3mobile.SharedPreferences

import android.content.Context
import ca.cegepgarneau.tp3mobile.data.model.User

class MyPreferences(context:Context) {
    val PREFERENCE_NAME = "SharedPreferenceTP3"
    val PREFERENCE_UserN = "UserName"
    val PREFERENCE_UserLN = "UserLastName"
    val PREFERENCE_UserId = "UserId"
    val PREFERENCE_UserE = "UserE"

    val preference = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    // obtiens le user connecté
    fun getLoginAccount(): User{
        val prenom = preference.getString(PREFERENCE_UserLN, "cegep").toString()
        val nom = preference.getString(PREFERENCE_UserN, "garneau").toString()
        val id = preference.getString(PREFERENCE_UserId, "").toString()
        val email = preference.getString(PREFERENCE_UserE, "").toString()
        return User(id,nom,prenom, email)
    }

    // initialisation du data dans le shared preference du user connecté
    fun setUser(user: User){
        val editor = preference.edit()
        editor.putString(PREFERENCE_UserLN, user.lastName)
        editor.putString(PREFERENCE_UserN, user.name)
        editor.putString(PREFERENCE_UserId, user.id)
        editor.putString(PREFERENCE_UserE, user.email)
        editor.apply()
    }

    // déconnexion d'un user
    fun logout() {
        val editor = preference.edit()
        editor.putString(PREFERENCE_UserE, "")
    }

}