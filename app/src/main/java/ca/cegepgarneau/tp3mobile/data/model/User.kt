package ca.cegepgarneau.tp3mobile.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// info d'un user
class User (

    val id: String,
    val name: String,
    val lastName: String,
    val email: String,
)
