package ca.cegepgarneau.tp3mobile.data.model

// info d'un message
data class Message(
    val id: Int,
    val author: String,
    val message: String,
    val lat: Double,
    val long: Double,
)